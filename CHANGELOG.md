# Changelog

<!--next-version-placeholder-->

## v0.5.1 (2024-08-15)

### Fix

* Fixed bugs in coordination sphere ([`14a9795`](https://gitlab.com/chilton-group/env_suite/-/commit/14a9795251b7ad412851f3f437eff48e76a26334))

## v0.5.0 (2024-07-05)



## v0.4.0 (2023-10-06)



## v0.3.0 (2023-09-20)

### Feature

* Added support form non-molecular crystals ([`86f61de`](https://gitlab.com/chilton-group/env_suite/-/commit/86f61ded9441e9551ed9d7d35ca500d4f82bc9ab))

## v0.2.1 (2023-08-22)

### Fix

* Documentation modules added ([`16b248f`](https://gitlab.com/chilton-group/env_suite/-/commit/16b248f1e084335a2b31f234d1b2d8539b4d3e6d))

## v0.2.0 (2023-08-22)



## v0.1.1 (2023-08-21)

### Fix

* Fixed documentation link in readme ([`deafaf1`](https://gitlab.com/chilton-group/env_suite/-/commit/deafaf1ea1540fd1dd32371065d498c7004052cb))

## v0.1.0 (2023-08-21)

### Feature

* Migrate cluster and charge frontends ([`e898a1d`](https://gitlab.com/chilton-group/env_suite/-/commit/e898a1dd91c57dd8284922efa6275add7189384d))
* Added guides to the docs and added the cli.py file and added some ([`a0eaf5d`](https://gitlab.com/chilton-group/env_suite/-/commit/a0eaf5d861a7a1857d6337f94ecc2fe2474a359b))
* Built the base of the docummentation and completed unit tests on ([`3858447`](https://gitlab.com/chilton-group/env_suite/-/commit/38584473d2b0b37230d2c7c3aa1866970f77eed2))
* Added initial ewald functions from vasp_suite to env_suite ([`32ade76`](https://gitlab.com/chilton-group/env_suite/-/commit/32ade76b4f23083fa51ea10c9dd4fa128a92b807))

### Fix

* Fixed missing import ([`dc5dab1`](https://gitlab.com/chilton-group/env_suite/-/commit/dc5dab1935bfd6d90d0565ea702cb048c072c712))
* Qm region region removed from sphere ([`356cda0`](https://gitlab.com/chilton-group/env_suite/-/commit/356cda04820fb4fb3a6650f57ae6aab8b1d85621))
* Spherical clusters ([`644ef50`](https://gitlab.com/chilton-group/env_suite/-/commit/644ef50e295f80ed8019ddd68646916dfeaac427))
* Fixed h5 and plot size ([`9a65f67`](https://gitlab.com/chilton-group/env_suite/-/commit/9a65f673917b058939e6ded46544edf2e6fd03f5))
* Made some corrections to have the coords in cartesian basis for ([`e74c2ae`](https://gitlab.com/chilton-group/env_suite/-/commit/e74c2aed8f3e9d2097e94e77bebc3eeeeead1ebd))
* Fixed bug issues, added plotter with optimisations ([`e127426`](https://gitlab.com/chilton-group/env_suite/-/commit/e127426fcad7ea1cb4a09c73949f2e2796f67d50))
* Added files for python-semantic-release ([`5605721`](https://gitlab.com/chilton-group/env_suite/-/commit/56057214d9da36691b8ff628c95a62ce3de2261a))

### Documentation

* Updated docs ([`92208d2`](https://gitlab.com/chilton-group/env_suite/-/commit/92208d25943e559c738b16c589b2759f9c278b7f))
* Added theory to the docs for the summation and fitting prcedure ([`9f16b58`](https://gitlab.com/chilton-group/env_suite/-/commit/9f16b58fb9bc6874aefdbb4d3f63c04917c6a2cc))

### Performance

* Increased performance ([`6fb10dd`](https://gitlab.com/chilton-group/env_suite/-/commit/6fb10dd4c6de7e109c23d33f4246ef4b27e127ad))
* Made some performance upgrades. added utility tools ([`97b3a60`](https://gitlab.com/chilton-group/env_suite/-/commit/97b3a60399fb6754a551257b42b44397d61a5d07))
