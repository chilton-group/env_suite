Installation
============

`env_suite` can be installed from PyPI using `pip`:

.. code:: bash

   pip install env_suite
