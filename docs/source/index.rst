.. env_suite documentation master file, created by
   sphinx-quickstart on Tue Jul  4 11:39:22 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Home
====

.. toctree::
   Installation <installation>
   Modules <modules/index>
   Guides <guide/index>
   Theory <theory/index>
   Contributors <contributors>
   :maxdepth: 3
   :caption: Contents:
   :hidden:

`env_suite` is a python package developed by the `Chilton Group <https://www.nfchilton.com>`_ for including 
enviromental effects in ab initio calculations
