Ewald Method
============

The Ewald method is used to embed the long-range infinite periodic potential
of a crystal into a finite cluster for Complete Active Space Self-Consistent
Field (CASSCF) calculations to obtain the electronic structure of the crystal.



Performing the Ewald calculation 
--------------------------------

The Ewald sum and charge fitting procedure is performed using the `ewald`
programme within `env_suite`. The programme requires the following files:

* `POSCAR` - The crystal structure in VASP POSCAR format.
             (Other formats such as .cif files will be supported in later updates)
* `charges.dat` - A file containing the charges of the atoms in the crystal.
                  The charges should be in the same order as the atoms in the
                  POSCAR file.

The programme can be run using the following command:

.. code:: bash

   # Calculate the Ewald potential from a central index
   env_suite ewald --cluster_expansion 3 3 3 --central_index 1 --from_central --poscar POSCAR --charges charges.dat --r_cut 15

   # Calculate the Ewald potential from multiple indecies (e.g. M2 or Ln2)
   env_suite ewald --cluster_expansion 3 3 3 --central_index 1 2 ... --from_central --poscar POSCAR --charges charges.dat --r_cut 15

   # Calculate the Ewald potential and simultaneously build a QM region from a non molecular crystal
   # Subsequently, can move some atoms from the QM region into the charge region to edit QM region description as required
   env_suite ewald --poscar POSCAR --cluster_expansion 4 4 4 --charges charges.dat --r_cut 20 --central_index 1 --atom_index Dy1 --coordination_sphere --coordination_sphere_number 3 --max_bond_length 2.5

   # Calculate the Ewald potential on a non molecular crystal using a preprepared qm region
   env_suite ewald --cluster_expansion 3 3 3 --from_central --qm_region 'qm.xyz' --charges charges.dat --r_cut 15

   # Calculate the ewald potential using a spherical cluster
   # More efficient for unit cells that are non_cubic 
   env_suite ewald --cluster_cutoff 40 --central_index 1 --from_central --poscar POSCAR --charges charges.dat --r_cut 15

   # A full list of arguments can be found by running:
   env_suite ewald -h

The `ewald` programme should be run as an interactive job or on a local machine.
Do not run the `ewald` programme on a login node. 


Evaluating the output
---------------------

To ensure that the Ewald programme has output sensible charges the potential
should be evaluated and plotted. The potential can be plotted using the 
`plot_potential` programme. The can be run using the following command:

.. code:: bash

   # Plot the potential
   env_suite plot_potential <xy_grid> 12 12 -s

The `xy_grid` argument difines the length of the grid in the xy plane in 
which the potential is projected onto. It is recomended that the grid is 
smaller than the r_cut value used in the Ewald calculation. The `np` argument
defines the number of points in the xy grid. The `nc` argument defines the 
number of contours in the potential plot. The `s` argument defines whether 
the plot should be shown in the gui or saved to a file. The default value is
False, which saves the plot to a file.

Visualising the zones
---------------------
The three zones created when running the Ewald programme can be visualised as 
an analysis tool for determining if the exact point charge region is larger in 
some areas than the parameter region. This issue often arrises from using the 
`--cluster_cutoff` keyword. 

The programme plots a 3D representation of the points in space colour coded by 
region

.. code:: bash

   # Visualise the zones
   env_suite visualise_ewald


