Ewald summation
===============

The Ewald summation calculates the electrostatic potential of a periodic
crystal. The sum is separated into the real-space, reciprocal-space, and
self-interaction terms. Such that for atom :math:`r_i` the potential is:

.. math::

   V_{r_i} = U^{r}_i + U^{m}_i + U^{s}_i

Where :math:`U^{r}_i` is the real-space sum, :math:`U^{m}_i` is the 
reciprocal-space sum, and :math:`U^{s}_i` is the self-interaction term.
The real-space sum is calculated by:

.. math::

   U^{r}_i = \sum^N_{j \neq i} \sum_n q_j \frac{erfc(\alpha r_{i,jn})}{r_{i,jn}}

Where :math:`N` is the number of atoms in the cluster, :math:`r_{i,jn}` is the 
distance between atom :math:`r_i` and its periodic image :math:`r_j` in the 
:math:`n` direction, and :math:`\alpha` is a parameter that controls the 
convergence of the real-space sum. The reciprocal-space sum is calculated by: 

.. math:: 

   U^{m}_i = \frac{1}{\pi V} \sum^{N}_{j \neq i} q_j \sum_{m} \frac{exp(-[\pi f_m / \alpha]^2)}{f^2_m} \times \cos(2 \pi f_m \cdot r_{i,j0})


Where :math:`V` is the volume of the unit cell, :math:`f_m` is the :math:`m^{th}` 
image of the reciprocal lattice vector, and :math:`r_{i,j0}` is the distance
between atom :math:`r_i` and its periodic image :math:`r_j` in the zeroth
direction. The self-interaction term is calculated by: 

.. math::

   U^{s}_i = -\frac{2 \alpha q_i}{\sqrt{\pi}}

The Ewald potential is mapped to each periodic image of the primitive cell. 
Using a provided cutoff radius, three zones are created: The 'QM' region,
the 'exact point charge' region, and the 'parameter' region. The 'QM' region
is typically one molecule and is treated in an ab initio approach. the 'exact' 
region are the exact (ChelpG) point charges of the atoms in the cluster calculated ab 
initio. The 'parameter' region is a region of non physical point charges that fit 
the potential of the 'QM' and 'exact' regions to the exact Ewald potential.


Fitting precedure
==================
The fitting procedure fits the set of parameter charges such that the potential in the exact 
point charge and QM region is the infinite Ewald potential. The equation that governs this 
relationship is given by:

.. math::

   \sum^N_k \frac{q_k + \Delta q_k}{r_{ik}} - V_{ewald} = 0 \\

   \sum^N_k \frac{q_k + \Delta q_k}{r_{ik}} = V_{ewald} \\

   \sum^N_k \frac{\Delta q_k}{r_{ik}} = V_{ewald} - \sum^N_k \frac{q_k}{r_{ik}}

explain

Conditions
----------

Along with this fitting condition, there are two further conditions to the fitting 
procedure which govern the charge and dipole moment conservation. In a periodic crystal 
the total dipole moment and the total charge is zero. These conditions must be conserved 
when choosing an arbitrary sized exact point charge sphere.


The dipole moment uses a fitting procedure to drive any dipole moment generated as an 
effect of choosing an arbitrary sized cutoff radius for the exact point charge region. 
In the fitting procedure, the parameter region is fit such that the total dipole moment 
is zero.

.. math::

   \sum^N_k q_k r_k + \sum^{N_{param}}_k \Delta q_k r_k = 0

This forms a set of linear equations in the form `Ax = b` such that:

.. math::

   r_k^T \cdot \Delta q_k = - \sum^N_k q_k r_k

The second condition is that charge neutrality is conserved such that:

.. math::

   \sum^N_k q_k + \Delta q_k = 0 

Fitting the parameter charges
-----------------------------

In order to preserve charge conservation the sum of the fitted parameter charges must be 
equal and opposite in magnitude to the sum of the exact point charges. This is achieved 
using a qr decomposition of the matrix `A` and constraining the fitted parameter charges.

.. math::

   \underbrace{\frac{1}{r_{ik}}}_{A} \underbrace{\Delta q_k}_{x} = \underbrace{V_{ewald} - \sum^N_k \frac{q_k}{r_{ik}}}_{b} 

A vector `c` is formed to satisfy the constrains such that the dot product of `c`, which is in principal
the identity vector, and the solution vector `x` is equal to the negative of the sum of the exact point charges.

.. math::

   c = [1, ..., 1]^T \\

.. math::

   c x = d \\

.. math::

   d = - \sum^N_k q_k

A qr decomposition of c is performed resulting in Q and R.

.. math::

   AQ = [A_1(N_c), A_2(N-N_c)] \\

.. math::

   AQQ^Tx = b

Where `N_c` is the number of conditions and `N` is the number of parameters. A new set of linear equations is 
formed using the qr decomposition of `A`.

.. math::

   A_2 x_2 = b_2 \\

.. math::

   b_2 = b_1 - A_1(R^{-T}d)

Finally the obtained value of `x_2` is used to reform the solution for `x`.

.. math::

   Q^{T}x = \begin{bmatrix} x_1(N_c) \\ x_2(N - N_c) \end{bmatrix} \\

.. math::

   x_1 = R^{-T}d \\

.. math::

   x = Q \cdot \begin{bmatrix} x_1(N_c) \\ x_2(N - N_c) \end{bmatrix} \\


The final charge is given by:

.. math::

   \sum^N_k q_k + x_k = 0

Evalulation of the reusults
---------------------------

The results of the two fitting procedures are evaluated using the RMSD values of the 
potential compared to the calculated ewald potential and the final dipole moment and the 
zero vector. 

