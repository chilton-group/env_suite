.. _theory:

Guides
======

.. toctree::
   :maxdepth: 1
   :hidden:

    Ewald <ewald_theory>


The mathematical basis on which the code is written
